import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { DB_URL, DB_OPTIONS } from './config/db'
import { UsersModule } from './models/users/users.module'
import { AuthModule } from './models/auth/auth.module'

@Module({
  imports: [
    UsersModule,
    AuthModule,
    MongooseModule.forRoot(DB_URL, DB_OPTIONS),
  ],
})
export class AppModule {}

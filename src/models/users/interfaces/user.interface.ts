import { Document } from 'mongoose'

export interface User extends Document {
  readonly name?: string
  readonly email: string
  readonly hash: string
  readonly role?: string
  readonly avatar?: string
  readonly isDeleted?: boolean

  readonly created?: Date
  readonly updated?: Date
}

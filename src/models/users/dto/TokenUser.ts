import { IsEmail, IsNotEmpty } from 'class-validator'

export class TokenUser {
  @IsEmail()
  email: string

  @IsNotEmpty({ message: '' })
  role: string

  @IsNotEmpty()
  name: string

  avatar: string
}

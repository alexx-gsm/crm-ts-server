import { IsEmail, IsNotEmpty, MinLength } from 'class-validator'

export class RegisterUser {
  @IsEmail()
  email: string

  @IsNotEmpty()
  @MinLength(6, { message: 'Password is too short' })
  password: string

  @IsNotEmpty({ message: '' })
  role: string

  @IsNotEmpty()
  name: string
}

import { IsEmail, IsNotEmpty, MinLength } from 'class-validator'

export class ValidateUser {
  @IsEmail()
  email: string

  @IsNotEmpty()
  @MinLength(6, { message: 'Password is too short' })
  password: string
}

import { Controller, Body, Post, UseGuards, Request } from '@nestjs/common'
import { AuthService } from './auth.service'
import { RegisterUser } from '../users/dto/RegisterUser'
import { LocalAuthGuard } from './guards/local-auth.guard'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  async register(@Body() registerUser: RegisterUser) {
    return this.authService.register(registerUser)
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user)
  }
}

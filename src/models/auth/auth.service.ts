import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import * as gravatar from 'gravatar'
import * as bcrypt from 'bcrypt'
import { UsersService } from '../users/users.service'
import { ValidateUser } from '../users/dto/ValidateUser'
import { RegisterUser } from '../users/dto/RegisterUser'
import { TokenUser } from '../users/dto/TokenUser'

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser({ email, password }: ValidateUser) {
    const user = await this.usersService.findOne(email)

    if (user && bcrypt.compare(password, user.hash)) {
      return user
    }

    throw new HttpException('User not fount', HttpStatus.FORBIDDEN)
  }

  async register(registerUser: RegisterUser) {
    const user = await this.usersService.findOne(registerUser.email)

    if (user) {
      throw new HttpException('Email is registered', HttpStatus.FORBIDDEN)
    }

    const avatar = gravatar.url(registerUser.email, {
      s: '200', // Size
      r: 'pg', // Rating
      d: 'mm', // Default
    })

    const hash = await new Promise((resolve, reject) => {
      bcrypt.hash(registerUser.password, 10, function(err, hash) {
        if (err) reject(err)
        resolve(hash)
      })
    })

    try {
      await this.usersService.save({
        ...registerUser,
        avatar,
        hash,
      })

      return 'ok'
    } catch (e) {
      throw new HttpException('Server error', HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  async login({ name, email, role, avatar }: TokenUser) {
    return {
      token: `Bearer ${this.jwtService.sign({ name, email, role, avatar })}`,
    }
  }
}
